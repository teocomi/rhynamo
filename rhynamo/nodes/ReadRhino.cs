﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Autodesk.DesignScript.Runtime;
using Autodesk.DesignScript.Geometry;

using Rhino.FileIO;
using Rhino.Geometry;

namespace Interoperability.ReadRhino
{
  /// <summary>
  /// Get the Rhino File3dm Object
  /// </summary>
  public static class OpenRhino3dmModel
  {
    /// <summary>
    /// Gets a Rhino file object using a path string.
    /// </summary>
    /// <param name="RhinoFile">The file or path of the Rhino file (*.3dm)</param>
    /// <returns name="Model">The File3dm object (Rhino file)</returns>
    /// <returns name="Name">Name of the Rhino File</returns>
    /// <returns name="Bytes">Size of the Rhino file in Bytes</returns>
    /// <returns name="Created">Time created</returns>
    /// <returns name="CreatedBy">Creator of the Rhino file</returns>
    /// <returns name="LastEdited">Time the file was last edited</returns>
    /// <returns name="LastEditedBy">Name of the last person who edited the file.</returns>
    /// <returns name="Revision">Time of the last revision</returns>
    /// <returns name="Units">Units of the Rhino file</returns>
    /// <returns name="Units">Notes in the Rhino file</returns>
    /// <search>case,rhino,model,3dm,rhynamo</search>
    [MultiReturn(new[] { "RhinoModel", "Name", "Bytes", "Created", "CreatedBy", "LastEdited", "LastEditedBy", "Revision", "Units", "Notes" })]
    public static Dictionary<string, object> Get_RhinoFile(Object RhinoFile)
    {
      File3dm m_modelfile = null;
      string m_name = null;
      string m_size = null;
      string m_created = null;
      string m_createdby = null;
      string m_edited = null;
      string m_editedby = null;
      string m_revision = null;
      string m_units = null;
      string m_notes = null;

      if (RhinoFile is System.IO.FileInfo)
      {
        System.IO.FileInfo m_fileinfo = (System.IO.FileInfo)RhinoFile;

        m_modelfile = File3dm.Read(m_fileinfo.FullName);
        m_name = m_fileinfo.Name;
        m_size = m_fileinfo.Length.ToString();
        m_created = m_fileinfo.CreationTimeUtc.ToString();
        m_createdby = m_modelfile.CreatedBy;
        m_edited = m_fileinfo.LastWriteTimeUtc.ToString();
        m_editedby = m_modelfile.LastEditedBy;
        m_revision = m_modelfile.Revision.ToString();
        m_units = m_modelfile.Settings.ModelUnitSystem.ToString();
        m_notes = m_modelfile.Notes.Notes;
      }
      else if (RhinoFile is string)
      {
        // get file info
        System.IO.FileInfo m_fileinfo = new System.IO.FileInfo((string)RhinoFile);

        m_modelfile = File3dm.Read(m_fileinfo.FullName);
        m_name = m_fileinfo.Name;
        m_size = m_fileinfo.Length.ToString();
        m_created = m_fileinfo.CreationTimeUtc.ToString();
        m_createdby = m_modelfile.CreatedBy;
        m_edited = m_fileinfo.LastWriteTimeUtc.ToString();
        m_editedby = m_modelfile.LastEditedBy;
        m_revision = m_modelfile.Revision.ToString();
        m_units = m_modelfile.Settings.ModelUnitSystem.ToString();
        m_notes = m_modelfile.Notes.Notes;
      }

      // return model and info
      return new Dictionary<string, object>
              {
                  { "RhinoModel", m_modelfile },
                  { "Name", m_name },
                  { "Bytes", m_size },
                  { "Created", m_created },
                  { "CreatedBy", m_createdby },
                  { "LastEdited", m_edited },
                  { "LastEditedBy", m_editedby },
                  { "Revision", m_revision },
                  { "Units", m_units },
                  { "Notes", m_notes }
              };
    }
  }

  /// <summary>
  /// Get Rhino Object Types
  /// </summary>
  public static class RhinoObject
  {
    /// <summary>
    /// Gets Rhino document objects (file3dmobjects, dimension styles, hatch patterns, layers, linetypes, materials, views)
    /// </summary>
    /// <param name="RhinoModel">The Rhino model object</param>
    /// <returns name="RhinoObjects">Geometry Objects in the Rhino file.</returns>
    ///<returns name="HatchPatterns">Hatch patterns.</returns>
    ///<returns name="Layers">Layers</returns>
    ///<returns name="Linetypes">Linetypes</returns>
    ///<returns name="Materials">materials</returns>
    ///<returns name="Views">Views</returns>
    ///<search>case,rhino,model,3dm,rhynamo</search>
    [MultiReturn(new[] { "RhinoObjects", "HatchPatterns", "Layers", "Linetypes", "Materials", "Views" })]
    public static Dictionary<string, object> Get_DocumentObjects(File3dm RhinoModel)
    {
      List<Rhino.DocObjects.HatchPattern> m_hatchpatterns = RhinoModel.HatchPatterns.ToList();
      List<Rhino.DocObjects.Layer> m_layers = RhinoModel.Layers.ToList();
      List<Rhino.DocObjects.Linetype> m_linetypes = RhinoModel.Linetypes.ToList();
      List<Rhino.DocObjects.Material> m_materials = RhinoModel.Materials.ToList();
      List<Rhino.DocObjects.ViewInfo> m_views = RhinoModel.Views.ToList();

      // objects
      List<File3dmObject> m_objslist = new List<File3dmObject>();
      List<string> m_names = new List<string>();

      // search through each layer
      foreach (Rhino.DocObjects.Layer lay in RhinoModel.Layers)
      {
        File3dmObject[] m_objs = RhinoModel.Objects.FindByLayer(lay.Name);
        foreach (File3dmObject obj in m_objs)
        {
          m_objslist.Add(obj);
        }
      }

      // return info
      return new Dictionary<string, object>
              {
                  { "RhinoObjects", m_objslist },
                  { "HatchPatterns", m_hatchpatterns },
                  { "Layers", m_layers },
                  { "Linetypes", m_linetypes },
                  { "Materials", m_materials },
                  { "Views", m_views }
              };
    }

    /// <summary>
    /// Gets Rhino all Rhino 3dm Objects
    /// </summary>
    /// <param name="RhinoModel">The Rhino model object (File3dm)</param>
    /// <returns name="RhinoObjects">The list of Rhino objects.</returns>
    /// <search>case,rhino,model,3dm,rhynamo</search>
    public static File3dmObject[] Get_RhinoObjects(File3dm RhinoModel)
    {
      List<File3dmObject> m_objslist = new List<File3dmObject>();
      List<string> m_names = new List<string>();

      // search through each layer
      foreach (Rhino.DocObjects.Layer lay in RhinoModel.Layers)
      {
        File3dmObject[] m_objs = RhinoModel.Objects.FindByLayer(lay.Name);
        foreach (File3dmObject obj in m_objs)
        {
          m_objslist.Add(obj);
        }
      }

      // return info
      return m_objslist.ToArray();
    }

    /// <summary>
    /// Gets Rhino File3dmObjects from a model using a layer name.
    /// </summary>
    /// <param name="RhinoModel">The Rhino model object (File3dm)</param>
    /// <param name="layer">The name of the layer</param>
    /// <returns name="RhinoObjects">The list of Rhino objects.</returns>
    /// <search>case,rhino,model,3dm,rhynamo</search>
    public static File3dmObject[] Get_RhinoObjectsByLayer(File3dm RhinoModel, string layer)
    {
      File3dmObject[] objects = RhinoModel.Objects.FindByLayer(layer);
      // return info
      return objects;
    }

    /// <summary>
    /// Gets Rhino layer names.
    /// </summary>
    /// <param name="RhinoModel">The Rhino model object</param>
    /// <returns name="LayerNames">List of Rhino layer names.</returns>
    /// <search>case,rhino,model,layers,3dm,rhynamo</search>
    public static List<string> Get_RhinoLayerNames(File3dm RhinoModel)
    {
      List<string> m_names = new List<string>();
      foreach (Rhino.DocObjects.Layer lay in RhinoModel.Layers)
      {
        m_names.Add(lay.Name);
      }
      // return info
      return m_names;
    }
  }

  /// <summary>
  /// Get Rhino Object Attribute info
  /// </summary>
  public static class RhinoObjectAttributes
  {
    /// <summary>
    /// Rhino Hatch Pattern Attributes
    /// </summary>
    /// <param name="RhinoHatchPattern">Rhino Hatch Pattern</param>
    /// <returns name="Name">Name of the Hatch Pattern.</returns>
    /// <returns name="Index">Index of the Hatch Pattern</returns>
    /// <returns name="Description">Description.</returns>
    /// <returns name="FillType">Fill type.</returns>
    /// <search>case,rhino,model,3dm,rhynamo,hatch</search>
    [MultiReturn(new[] { "Name", "Index", "Description", "FillType" })]
    public static Dictionary<string, object> Get_HatchPatternAttributes(Rhino.DocObjects.HatchPattern RhinoHatchPattern)
    {
      string m_name = RhinoHatchPattern.Name;
      string m_index = RhinoHatchPattern.Index.ToString();
      string m_description = RhinoHatchPattern.Description;
      string m_filltype = RhinoHatchPattern.FillType.ToString();

      // return info
      return new Dictionary<string, object>
              {
                  { "Name", m_name },
                  { "Index", m_index },
                  { "Description", m_description },
                  { "FillType", m_filltype }
              };
    }

    /// <summary>
    /// Rhino Layer Attributes
    /// </summary>
    /// <param name="RhinoLayer">Rhino Layer</param>
    /// <returns name="Name">Layer Name</returns>
    /// <returns name="Index">Layer Index</returns>
    /// <returns name="LinetypeIndex">Linetype Index</returns>
    /// <returns name="MaterialIndex">Material Index</returns>
    /// <returns name="Color">Layer Color</returns>
    /// <returns name="PlotColor">Plot Color</returns>
    /// <returns name="GUID">GUID</returns>
    /// <search>case,rhino,model,3dm,rhynamo,layer</search>
    [MultiReturn(new[] { "Name", "Index", "LinetypeIndex", "MaterialIndex", "Color", "PlotColor", "GUID" })]
    public static Dictionary<string, object> Get_LayerAttributes(Rhino.DocObjects.Layer RhinoLayer)
    {
      string m_name = RhinoLayer.Name;
      int m_index = RhinoLayer.LayerIndex;
      int m_linetype = RhinoLayer.LinetypeIndex;
      int m_materialindex = RhinoLayer.RenderMaterialIndex;
      System.Drawing.Color m_color = RhinoLayer.Color;
      System.Drawing.Color m_plotcolor = RhinoLayer.PlotColor;
      string m_guid = RhinoLayer.Id.ToString();

      // return info
      return new Dictionary<string, object>
              {
                  { "Name", m_name },
                  { "Index", m_index },
                  { "LinetypeIndex", m_linetype },
                  { "MaterialIndex", m_materialindex},
                  { "Color", m_color },
                  { "PlotColor", m_plotcolor },
                  { "GUID", m_guid}
              };
    }

    /// <summary>
    /// Rhino Linetype Attributes
    /// </summary>
    /// <param name="RhinoLinetype">Rhino Linetype</param>
    /// <returns name="Name">Linetype Name</returns>
    /// <returns name="Index">Linetype Index</returns>
    /// <returns name="SegmentCount">Segment count</returns>
    /// <returns name="PatternLength">Pattern length</returns>
    /// <returns name="GUID">GUID</returns>
    /// <search>case,rhino,model,3dm,rhynamo,linetype</search>
    [MultiReturn(new[] { "Name", "Index", "SegmentCount", "PatternLength", "GUID" })]
    public static Dictionary<string, object> Get_LinetypeAttributes(Rhino.DocObjects.Linetype RhinoLinetype)
    {
      string m_name = RhinoLinetype.Name;
      int m_index = RhinoLinetype.LinetypeIndex;
      int m_segmentcount = RhinoLinetype.SegmentCount;
      double m_patternlength = RhinoLinetype.PatternLength;
      string m_guid = RhinoLinetype.Id.ToString();

      // return info
      return new Dictionary<string, object>
              {
                  { "Name", m_name },
                  { "Index", m_index },
                  { "SegmentCount", m_segmentcount },
                  { "PatternLength", m_patternlength},
                  { "GUID", m_guid }
              };
    }

    /// <summary>
    /// Rhino Material Attributes
    /// </summary>
    /// <param name="RhinoMaterial">Rhino Material</param>
    /// <returns name="Name">Material Name</returns>
    /// <returns name="Index">Material Index</returns>
    /// <returns name="IndexOfRefraction">Index of Refraction</returns>
    /// <returns name="Reflectivity">Reflectivity</returns>
    /// <returns name="Transparency">Transparency</returns>
    /// <returns name="AmbientColor">Ambient Color (as System.Color)</returns>
    /// <returns name="DiffuseColor">Diffuse Color (as System.Color)</returns>
    /// <returns name="ReflectionColor">Reflection Color (as System.Color)</returns>
    /// <returns name="RefractionColor">Refraction Color (as System.Color)</returns>
    /// <returns name="TransparentColor">Transparent Color (as System.Color)</returns>
    /// <returns name="GUID">GUID</returns>
    /// <search>case,rhino,model,3dm,rhynamo,material</search>
    [MultiReturn(new[] { "Name", "Index", "IndexOfRefraction", "Reflectivity", "Transparency", "AmbientColor", "DiffuseColor", "ReflectionColor", "RefractionColor", "TransparentColor", "GUID" })]
    public static Dictionary<string, object> Get_MaterialAttributes(Rhino.DocObjects.Material RhinoMaterial)
    {
      string m_name = RhinoMaterial.Name;
      int m_index = RhinoMaterial.MaterialIndex;
      double m_intexrefract = RhinoMaterial.IndexOfRefraction;
      double m_reflectivity = RhinoMaterial.Reflectivity;
      double m_transparency = RhinoMaterial.Transparency;
      System.Drawing.Color m_ambientcol = RhinoMaterial.AmbientColor;
      System.Drawing.Color m_diffusecol = RhinoMaterial.DiffuseColor;
      System.Drawing.Color m_reflectioncol = RhinoMaterial.ReflectionColor;
      System.Drawing.Color m_refractioncol = RhinoMaterial.ReflectionColor;
      System.Drawing.Color m_transparentcol = RhinoMaterial.TransparentColor;
      string m_guid = RhinoMaterial.Id.ToString();

      // return info
      return new Dictionary<string, object>
              {
                  { "Name", m_name },
                  { "Index", m_index },
                  { "IndexOfRefraction", m_intexrefract },
                  { "Reflectivty", m_reflectivity },
                  { "Transparency", m_transparency },
                  { "AmbientColor", m_ambientcol },
                  { "DiffuseColor", m_diffusecol },
                  { "ReflectionColor", m_reflectioncol },
                  { "RefractionColor", m_refractioncol },
                  { "TransparentColor", m_transparentcol },
                  { "GUID", m_guid }
              };
    }

    /// <summary>
    /// Rhino View Attributes
    /// </summary>
    /// <param name="RhinoView">Rhino Linetype</param>
    /// <returns name="Name">Material Name</returns>
    /// <returns name="Index">Material Index</returns>
    /// <returns name="Location">Location (as Point)</returns>
    /// <returns name="Target">Target (as Point)</returns>
    /// <returns name="LensLength">Lens Length</returns>
    /// <returns name="GUID">GUID</returns>
    /// <search>case,rhino,model,3dm,rhynamo,view</search>
    [MultiReturn(new[] { "Name", "Index", "Location", "Target", "LensLength", "GUID" })]
    public static Dictionary<string, object> Get_ViewAttributes(Rhino.DocObjects.ViewInfo RhinoView)
    {
      string m_name = RhinoView.Name;
      Autodesk.DesignScript.Geometry.Point m_location = Autodesk.DesignScript.Geometry.Point.ByCoordinates(RhinoView.Viewport.CameraLocation.X, RhinoView.Viewport.CameraLocation.Y, RhinoView.Viewport.CameraLocation.Z);
      Autodesk.DesignScript.Geometry.Point m_target = Autodesk.DesignScript.Geometry.Point.ByCoordinates(RhinoView.Viewport.TargetPoint.X, RhinoView.Viewport.TargetPoint.Y, RhinoView.Viewport.TargetPoint.Z);
      double m_lens = RhinoView.Viewport.Camera35mmLensLength;
      string m_guid = RhinoView.Viewport.Id.ToString();

      // return info
      return new Dictionary<string, object>
              {
                  { "Name", m_name },
                  { "Location", m_location },
                  { "Target", m_target },
                  { "LensLength", m_lens },
                  { "GUID", m_guid },
              };
    }

    /// <summary>
    /// Gets Rhino object names
    /// </summary>
    /// <param name="RhinoObject">List of Rhino objects.</param>
    /// <returns name="Name">Geometry Name</returns>
    /// <returns name="LayerIndex">Layer Index</returns>
    /// <returns name="LinetypeIndex">Linetype Index</returns>
    /// <returns name="MaterialIndex">Material Index</returns>
    /// <returns name="Color">Object color</returns>
    /// <returns name="PlotColor">Plot color</returns>
    /// <returns name="ObjectType">Type of Rhino Object</returns>
    /// <returns name="GUID">GUID</returns>
    /// <search>case,rhino,model,names,3dm,rhynamo</search>
    [MultiReturn(new[] { "Name", "LayerIndex", "LinetypeIndex", "MaterialIndex", "Color", "PlotColor", "ObjectType", "GUID" })]
    public static Dictionary<string, object> Get_RhinoGeometryAttributes(File3dmObject RhinoObject)
    {
      string m_name = RhinoObject.Name;
      int m_layerindex = RhinoObject.Attributes.LayerIndex;
      int m_linetypeindex = RhinoObject.Attributes.LinetypeIndex;
      int m_materialindex = RhinoObject.Attributes.MaterialIndex;
      System.Drawing.Color m_objcolor = RhinoObject.Attributes.ObjectColor;
      System.Drawing.Color m_objplotcolor = RhinoObject.Attributes.PlotColor;
      string m_guid = RhinoObject.Attributes.ObjectId.ToString();
      string m_type = RhinoObject.Geometry.ObjectType.ToString();

      // return info
      return new Dictionary<string, object>
              {
                  { "Name", m_name },
                  { "LayerIndex", m_layerindex },
                  { "LinetypeIndex", m_linetypeindex },
                  { "MaterialIndex", m_materialindex },
                  { "Color", m_objcolor },
                  { "PlotColor", m_objplotcolor },
                  { "ObjectType", m_type },
                  { "GUID", m_guid },
              };
    }

    /// <summary>
    /// Get Custom User Dictionary Data from Rhino 3DM objects
    /// </summary>
    /// <param name="RhinoObject">Rhino File3dmObject</param>
    /// <returns name="UserDictionary">User Dictionary object</returns>
    /// <returns name="Keys">Dictionary Keys</returns>
    /// <returns name="Values">Dictionary values</returns>
    /// <search>case,rhino,model,user,dictionary,3dm,rhynamo</search>
    [MultiReturn(new[] { "UserDictionary", "Keys", "Values" })]
    public static Dictionary<string, object> Get_UserDictionary(File3dmObject RhinoObject)
    {
      Rhino.Collections.ArchivableDictionary m_dictionary = RhinoObject.Geometry.UserDictionary;
      object[] m_values = null;
      string[] m_keys = null;

      if (m_dictionary != null)
      {
        m_values = RhinoObject.Attributes.UserDictionary.Values;
        m_keys = RhinoObject.Attributes.UserDictionary.Keys;
      }

      // return info
      return new Dictionary<string, object>
              {
                  { "UserDictionary", m_dictionary },
                  { "Keys", m_keys },
                  { "Values", m_values }
              };
    }

    /// <summary>
    /// Returns a value from a Rhino UserDictionary based on a Key
    /// </summary>
    /// <param name="RhinoDictionary">Rhino UserDictionary</param>
    /// <param name="key">Key value</param>
    /// <returns name="Value">Material Name</returns>
    /// <search>case,rhino,model,user, dictionary,3dm,rhynamo</search>
    public static object Get_UserDictionaryValue(Rhino.Collections.ArchivableDictionary RhinoDictionary, string key)
    {
      // get a dictionary value based on a key
      object m_value = null;

      if (RhinoDictionary != null)
      {
        RhinoDictionary.TryGetValue(key, out m_value);
      }
      
      // return info
      return m_value;
    }

    /// <summary>
    /// Returns a Rhino UserStrings
    /// </summary>
    /// <param name="RhinoObject">Rhino File3dmObject</param>
    /// <returns name="Keys">User string keys.</returns>
    /// <returns name="Values">User string values.</returns>
    [MultiReturn(new[] { "Keys", "Values"})]
    public static Dictionary<string, object> Get_UserStrings(File3dmObject RhinoObject)
    {
      
      System.Collections.Specialized.NameValueCollection m_userstrings = RhinoObject.Geometry.GetUserStrings();
      List<List<string>> m_values = new List<List<string>>();
      string[] m_keys = null;

      if (m_userstrings != null)
      {
        m_keys = m_userstrings.AllKeys;
        foreach (string k in m_keys)
        {
          List<string> m_vals = m_userstrings.GetValues(k).ToList();
          m_values.Add(m_vals);
        }
      }

      // return info
      return new Dictionary<string, object>
              {
                  { "Keys", m_keys },
                  { "Values", m_values }
              };

    }

    /// <summary>
    /// Returns a value from a Rhino UserDictionary based on a Key
    /// </summary>
    /// <param name="RhinoObject">Rhino File3dmObject</param>
    /// <param name="key">Key value</param>
    /// <returns name="Value">UserDictionary Value</returns>
    /// <search>case,rhino,model,user, dictionary,3dm,rhynamo</search>
    public static object Get_UserStringValue(File3dmObject RhinoObject, string key)
    {
      // get a user string value
      object m_value = RhinoObject.Geometry.GetUserString(key);
      return m_value;
    }
  }

  /// <summary>
  /// Convert Rhino Geometry
  /// </summary>
  public static class RhinoTranslationToDS
  {

    /// <summary>
    /// Converts Rhino objects to Dynamo geometry
    /// </summary>
    /// <param name="RhinoObjects">List of Rhino Objects</param>
    /// <returns name="Points">Dynamo Point</returns>
      /// <returns name="Curves">Dynamo Curves</returns>
      /// <returns name="Breps">Dynamo PolySurfaces</returns>
      /// <returns name="Extrusions">Dynamo PolySurfaces</returns>
      /// <returns name="Meshes">Dynamo Meshes</returns>
    /// <search>case,rhino,model,3dm,geometry,convert,rhynamo</search>
    [MultiReturn(new[] { "Points", "Curves", "Breps", "Extrusions", "Meshes" })]
    public static Dictionary<string, object> Rhino_AllGeometryToDS(File3dmObject[] RhinoObjects)
    {
      // rhino curve list
      List<Rhino.Geometry.Point> rh_points = new List<Rhino.Geometry.Point>();
      List<Rhino.Geometry.Curve> rh_curves = new List<Rhino.Geometry.Curve>();
      List<Rhino.Geometry.Brep> rh_breps = new List<Rhino.Geometry.Brep>();
      List<Rhino.Geometry.Extrusion> rh_extrusions = new List<Rhino.Geometry.Extrusion>();
      List<Rhino.Geometry.Mesh> rh_meshes = new List<Rhino.Geometry.Mesh>();

      foreach (File3dmObject obj in RhinoObjects)
      {
        GeometryBase geo = obj.Geometry;

        // check if the geometry is a Point
        if (geo is Rhino.Geometry.Point)
        {
          // add point to Rhino points list
          Rhino.Geometry.Point pt = geo as Rhino.Geometry.Point;
          rh_points.Add(pt);
        }

        // check if geometry is a curve
        if (geo is Rhino.Geometry.Curve)
        {
          // add curve to list
          Rhino.Geometry.Curve ln = geo as Rhino.Geometry.Curve;
          rh_curves.Add(ln);
        }

        // check if geometry is a brep
        if (geo is Rhino.Geometry.Brep)
        {
          Rhino.Geometry.Brep rh_brep = geo as Rhino.Geometry.Brep;

          rh_breps.Add(rh_brep);
        }

        // get breps
        if (geo is Rhino.Geometry.Extrusion)
        {
          Rhino.Geometry.Extrusion rh_ext = geo as Rhino.Geometry.Extrusion;

          rh_extrusions.Add(rh_ext);
        }

        // check if geometry is a mesh
        if (geo is Rhino.Geometry.Mesh)
        {
          Rhino.Geometry.Mesh rh_mesh = geo as Rhino.Geometry.Mesh;
          rh_meshes.Add(rh_mesh);
        }
      }

      // conversion to design script
      Rhynamo.classes.clsGeometryConversionUtils rh_ds = new Rhynamo.classes.clsGeometryConversionUtils();

      List<Autodesk.DesignScript.Geometry.Point> ds_points = rh_ds.Convert_PointsToDS(rh_points);
      List<Autodesk.DesignScript.Geometry.Curve> ds_curves = rh_ds.Convert_CurvesToDS(rh_curves);
      List<Autodesk.DesignScript.Geometry.PolySurface> ds_breps = rh_ds.Convert_BrepsToDS(rh_breps);
      List<Autodesk.DesignScript.Geometry.PolySurface> ds_extrusions = rh_ds.Convert_ExtrusionToDS(rh_extrusions);
      List<Autodesk.DesignScript.Geometry.Mesh> ds_meshes = rh_ds.Convert_MeshToDS(rh_meshes);

      return new Dictionary<string, object>
              {
                  { "Points", ds_points },
                  { "Curves", ds_curves },
                  { "Breps", ds_breps },
                  { "Extrusions", ds_extrusions },
                  { "Meshes", ds_meshes }
              };
    }

    /// <summary>
    /// Converts Rhino arc objects to Dynamo arcs.
    /// </summary>
    /// <param name="RhinoObjects">List of Rhino objects to search through</param>
    /// <returns name="Geometry">Dynamo Geometry</returns>
    /// <returns name="Name">Object Name</returns>
    /// <returns name="LayerIndex">Layer Index</returns>
    /// <returns name="Color">Object color (as System.Color)</returns>
    /// <returns name="GUID">GUID</returns>
    /// <search>case,rhino,model,3dm,arc,convert,rhynamo</search>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static Dictionary<string, object> Rhino_ArcsToDS(File3dmObject[] RhinoObjects)
    {
      //List of Rhino lines
      List<Rhino.Geometry.ArcCurve> rh_arcs = new List<Rhino.Geometry.ArcCurve>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in RhinoObjects)
      {
        GeometryBase geo = obj.Geometry;

        // check if geometry is an arc curve
        if (geo is Rhino.Geometry.ArcCurve)
        {
          // add arc curve to arc list
          Rhino.Geometry.ArcCurve arc = geo as Rhino.Geometry.ArcCurve;
          rh_arcs.Add(arc);

          // attributes
          m_names.Add(obj.Attributes.Name);
          m_layerindeces.Add(obj.Attributes.LayerIndex);
          m_colors.Add(obj.Attributes.ObjectColor);
          m_guids.Add(obj.Attributes.ObjectId.ToString());
        }
      }

      // convert Rhino arcs to DesignScript arcs
      Rhynamo.classes.clsGeometryConversionUtils rh_ds = new Rhynamo.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.Arc> ds_arcs = rh_ds.Convert_ArcsToDS(rh_arcs);

      return new Dictionary<string, object>
              {
                  { "Geometry", ds_arcs },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Converts Rhino Breps to Dynamo PolySurface objects
    /// </summary>
    /// <param name="RhinoObjects">List of Rhino objects to search through.</param>
    /// <returns name="Geometry">Dynamo Geometry</returns>
    /// <returns name="Name">Object Name</returns>
    /// <returns name="LayerIndex">Layer Index</returns>
    /// <returns name="Color">Object color (as System.Color)</returns>
    /// <returns name="GUID">GUID</returns>
    /// <search>case,rhino,model,3dm,nurbs,brep,convert,rhynamo</search>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static Dictionary<string, object> Rhino_BrepsToDS(File3dmObject[] RhinoObjects)
    {
      // list of rhino nurbs surfaces
      List<Rhino.Geometry.Brep> rh_breps = new List<Rhino.Geometry.Brep>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in RhinoObjects)
      {
        GeometryBase geo = obj.Geometry;

        // get breps
        if (geo is Rhino.Geometry.Brep)
        {
          Rhino.Geometry.Brep rh_brep = geo as Rhino.Geometry.Brep;

          rh_breps.Add(rh_brep);

          // attributes
          m_names.Add(obj.Attributes.Name);
          m_layerindeces.Add(obj.Attributes.LayerIndex);
          m_colors.Add(obj.Attributes.ObjectColor);
          m_guids.Add(obj.Attributes.ObjectId.ToString());
        }
      }

      // convert Rhino Nurbs Surface to DesignScript Nurbs Surface
      Rhynamo.classes.clsGeometryConversionUtils rh_ds = new Rhynamo.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.PolySurface> ds_polysurfaces = rh_ds.Convert_BrepsToDS(rh_breps);

      return new Dictionary<string, object>
              {
                  { "Geometry", ds_polysurfaces },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Converts Rhino circle objects to Dynamo circles
    /// </summary>
    /// <param name="RhinoObjects">Rhino circle objects</param>
    /// <returns name="Geometry">Dynamo Geometry</returns>
    /// <returns name="Name">Object Name</returns>
    /// <returns name="LayerIndex">Layer Index</returns>
    /// <returns name="Color">Object color (as System.Color)</returns>
    /// <returns name="GUID">GUID</returns>
    /// <search>case,rhino,model,3dm,nurbs,circle,convert,rhynamo</search>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static Dictionary<string, object> Rhino_CirclesToDS(List<File3dmObject> RhinoObjects)
    {
      List<Rhino.Geometry.Curve> rh_circles = new List<Rhino.Geometry.Curve>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in RhinoObjects)
      {
        GeometryBase geo = obj.Geometry;
        if (geo is Rhino.Geometry.Curve)
        {
          Rhino.Geometry.Curve crv = (Rhino.Geometry.Curve)geo;

          // determine if curve is a circle
          if (crv.IsCircle() == true)
          {
            rh_circles.Add(crv);

            // attributes
            m_names.Add(obj.Attributes.Name);
            m_layerindeces.Add(obj.Attributes.LayerIndex);
            m_colors.Add(obj.Attributes.ObjectColor);
            m_guids.Add(obj.Attributes.ObjectId.ToString());
          }
        }
      }
      // create designscript circles
      Rhynamo.classes.clsGeometryConversionUtils rh_ds = new Rhynamo.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.Circle> ds_circles = rh_ds.Convert_CirclesToDS(rh_circles);

      return new Dictionary<string, object>
              {
                  { "Geometry", ds_circles },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Converts Rhino curve objects to Dynamo curves (lines, arcs, nurbs, polyline, polycurve)
    /// </summary>
    /// <param name="RhinoObjects">List of Rhino objects to search through.</param>
    /// <returns name="Geometry">Dynamo Geometry</returns>
    /// <returns name="Name">Object Name</returns>
    /// <returns name="LayerIndex">Layer Index</returns>
    /// <returns name="Color">Object color (as System.Color)</returns>
    /// <returns name="GUID">GUID</returns>
    /// <search>case,rhino,model,curve,3dm,rhynamo</search>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static new Dictionary<string, object> Rhino_CurvesToDS(File3dmObject[] RhinoObjects)
    {
      // rhino curve list
      List<Rhino.Geometry.Curve> rh_curves = new List<Rhino.Geometry.Curve>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in RhinoObjects)
      {
        GeometryBase geo = obj.Geometry;

        // check if geometry is a curve
        if (geo is Rhino.Geometry.Curve)
        {
          // add curve to list
          Rhino.Geometry.Curve ln = geo as Rhino.Geometry.Curve;
          rh_curves.Add(ln);

          // attributes
          m_names.Add(obj.Attributes.Name);
          m_layerindeces.Add(obj.Attributes.LayerIndex);
          m_colors.Add(obj.Attributes.ObjectColor);
          m_guids.Add(obj.Attributes.ObjectId.ToString());
        }
      }

      // conversion to design script
      Rhynamo.classes.clsGeometryConversionUtils rh_ds = new Rhynamo.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.Curve> ds_curves = rh_ds.Convert_CurvesToDS(rh_curves);

      // return curve objects
      return new Dictionary<string, object>
              {
                  { "Geometry", ds_curves },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Converts Rhino Extrusions to Dynamo PolySurface objects
    /// </summary>
    /// <param name="RhinoObjects">List of Rhino objects to search through.</param>
    /// <returns name="Geometry">Dynamo Geometry</returns>
    /// <returns name="Name">Object Name</returns>
    /// <returns name="LayerIndex">Layer Index</returns>
    /// <returns name="Color">Object color (as System.Color)</returns>
    /// <returns name="GUID">GUID</returns>
    /// <search>case,rhino,model,3dm,nurbs,extrusion,convert,rhynamo</search>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static Dictionary<string, object> Rhino_ExtrusionToDS(File3dmObject[] RhinoObjects)
    {
      // list of rhino nurbs surfaces
      List<Rhino.Geometry.Extrusion> rh_extrusion = new List<Rhino.Geometry.Extrusion>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in RhinoObjects)
      {
        GeometryBase geo = obj.Geometry;

        // get breps
        if (geo is Rhino.Geometry.Extrusion)
        {
          Rhino.Geometry.Extrusion rh_brep = geo as Rhino.Geometry.Extrusion;

          rh_extrusion.Add(rh_brep);

          // attributes
          m_names.Add(obj.Attributes.Name);
          m_layerindeces.Add(obj.Attributes.LayerIndex);
          m_colors.Add(obj.Attributes.ObjectColor);
          m_guids.Add(obj.Attributes.ObjectId.ToString());
        }
      }

      // convert Rhino Nurbs Surface to DesignScript Nurbs Surface
      Rhynamo.classes.clsGeometryConversionUtils rh_ds = new Rhynamo.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.PolySurface> ds_polysurfaces = rh_ds.Convert_ExtrusionToDS(rh_extrusion);

      return new Dictionary<string, object>
              {
                  { "Geometry", ds_polysurfaces },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Converts Rhino line objects to Dynamo lines.
    /// </summary>
    /// <param name="RhinoObjects">List of Rhino objects to search through</param>
    /// <returns name="Geometry">Dynamo Geometry</returns>
    /// <returns name="Name">Object Name</returns>
    /// <returns name="LayerIndex">Layer Index</returns>
    /// <returns name="Color">Object color (as System.Color)</returns>
    /// <returns name="GUID">GUID</returns>
    /// <returns name="lines">DesignScript Lines</returns>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static Dictionary<string, object> Rhino_LinesToDS(File3dmObject[] RhinoObjects)
    {
      //List of Rhino lines
      List<Rhino.Geometry.LineCurve> rh_lines = new List<Rhino.Geometry.LineCurve>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in RhinoObjects)
      {
        GeometryBase geo = obj.Geometry;

        // check if geometry is a line curve
        if (geo is Rhino.Geometry.LineCurve)
        {
          // add line curve to lines list
          Rhino.Geometry.LineCurve ln = geo as Rhino.Geometry.LineCurve;
          rh_lines.Add(ln);

          // attributes
          m_names.Add(obj.Attributes.Name);
          m_layerindeces.Add(obj.Attributes.LayerIndex);
          m_colors.Add(obj.Attributes.ObjectColor);
          m_guids.Add(obj.Attributes.ObjectId.ToString());
        }
      }

      // convert Rhino lines to DesignScript lines
      Rhynamo.classes.clsGeometryConversionUtils rh_ds = new Rhynamo.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.Line> ds_lines = rh_ds.Convert_LinesToDS(rh_lines);

      return new Dictionary<string, object>
              {
                  { "Geometry", ds_lines },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Convert Rhino meshes to Dynamo meshes
    /// </summary>
    /// <param name="RhinoObjects">Rhino mesh objects</param>
    /// <returns name="Geometry">Dynamo Geometry</returns>
    /// <returns name="Name">Object Name</returns>
    /// <returns name="LayerIndex">Layer Index</returns>
    /// <returns name="Color">Object color (as System.Color)</returns>
    /// <returns name="GUID">GUID</returns>
    /// <search>case,rhino,model,3dm,nurbs,mesh,convert,rhynamo</search>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static Dictionary<string, object> Rhino_MeshToDS(File3dmObject[] RhinoObjects)
    {
      List<Rhino.Geometry.Mesh> rh_meshes = new List<Rhino.Geometry.Mesh>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in RhinoObjects)
      {
        GeometryBase geo = obj.Geometry;
        if (geo is Rhino.Geometry.Mesh)
        {
          Rhino.Geometry.Mesh rh_mesh = geo as Rhino.Geometry.Mesh;
          rh_meshes.Add(rh_mesh);

          // attributes
          m_names.Add(obj.Attributes.Name);
          m_layerindeces.Add(obj.Attributes.LayerIndex);
          m_colors.Add(obj.Attributes.ObjectColor);
          m_guids.Add(obj.Attributes.ObjectId.ToString());
        }
      }
      Rhynamo.classes.clsGeometryConversionUtils rh_ds = new Rhynamo.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.Mesh> ds_meshes = rh_ds.Convert_MeshToDS(rh_meshes);

      return new Dictionary<string, object>
              {
                  { "Geometry", ds_meshes },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Converts Rhino NurbsCurve objects to Dynamo NurbsCurves
    /// </summary>
    /// <param name="RhinoObjects">List of Rhino objects to search through.</param>
    /// <returns name="Geometry">Dynamo Geometry</returns>
    /// <returns name="Name">Object Name</returns>
    /// <returns name="LayerIndex">Layer Index</returns>
    /// <returns name="Color">Object color (as System.Color)</returns>
    /// <returns name="GUID">GUID</returns>
    /// <search>case,rhino,model,3dm,nurbs,curve,convert,rhynamo</search>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static Dictionary<string, object> Rhino_NurbsCurvesToDS(File3dmObject[] RhinoObjects)
    {
      // list of Rhino Nurbs curves
      List<Rhino.Geometry.NurbsCurve> rh_nurbs = new List<Rhino.Geometry.NurbsCurve>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in RhinoObjects)
      {
        GeometryBase geo = obj.Geometry;

        // check if the object is a nurbs curve
        if (geo is Rhino.Geometry.NurbsCurve)
        {
          // add nurbs curve to nurbs list
          Rhino.Geometry.NurbsCurve nurbs = geo as Rhino.Geometry.NurbsCurve;
          rh_nurbs.Add(nurbs);

          // attributes
          m_names.Add(obj.Attributes.Name);
          m_layerindeces.Add(obj.Attributes.LayerIndex);
          m_colors.Add(obj.Attributes.ObjectColor);
          m_guids.Add(obj.Attributes.ObjectId.ToString());
        }
      }

      //convert Rhino NURBS to DesignScript NURBS
      Rhynamo.classes.clsGeometryConversionUtils rh_ds = new Rhynamo.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.NurbsCurve> ds_nurbs = rh_ds.Convert_NurbsCurveToDS(rh_nurbs);

      return new Dictionary<string, object>
              {
                  { "Geometry", ds_nurbs },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Converts Untrimmed Rhino NurbsSurface objects to Dynamo NurbsSurfaces
    /// </summary>
    /// <param name="RhinoObjects">List of Rhino objects to search through</param>
    /// <returns name="Geometry">Dynamo Geometry</returns>
    /// <returns name="Name">Object Name</returns>
    /// <returns name="LayerIndex">Layer Index</returns>
    /// <returns name="Color">Object color (as System.Color)</returns>
    /// <returns name="GUID">GUID</returns>
    /// <search>case,rhino,model,3dm,nurbs,surface,convert,rhynamo</search>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static Dictionary<string, object> Rhino_NurbsSurfaceToDS(File3dmObject[] RhinoObjects)
    {
      // list of rhino nurbs surfaces
      List<Rhino.Geometry.NurbsSurface> rh_nurbs = new List<Rhino.Geometry.NurbsSurface>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in RhinoObjects)
      {
        GeometryBase geo = obj.Geometry;

        // get breps
        if (geo is Rhino.Geometry.Brep)
        {
          Rhino.Geometry.Brep rh_brep = geo as Rhino.Geometry.Brep;

          // get breps that are surfaces
          if (rh_brep.IsSurface == true)
          {
            // get nurbs surface
            Rhino.Geometry.NurbsSurface rh_srf = rh_brep.Faces[0].ToNurbsSurface();
            rh_nurbs.Add(rh_srf);

            // attributes
            m_names.Add(obj.Attributes.Name);
            m_layerindeces.Add(obj.Attributes.LayerIndex);
            m_colors.Add(obj.Attributes.ObjectColor);
            m_guids.Add(obj.Attributes.ObjectId.ToString());
          }
        }
      }

      // convert Rhino Nurbs Surface to DesignScript Nurbs Surface
      Rhynamo.classes.clsGeometryConversionUtils rh_ds = new Rhynamo.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.NurbsSurface> ds_nurbs = rh_ds.Convert_NurbsSurfaceToDS(rh_nurbs);

      return new Dictionary<string, object>
              {
                  { "Geometry", ds_nurbs },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Converts Rhino Polyline objects to Dynamo Polyline
    /// </summary>
    /// <param name="RhinoObjects">List of Rhino objects to search through.</param>
    /// <returns name="Geometry">Dynamo Geometry</returns>
    /// <returns name="Name">Object Name</returns>
    /// <returns name="LayerIndex">Layer Index</returns>
    /// <returns name="Color">Object color (as System.Color)</returns>
    /// <returns name="GUID">GUID</returns>
    /// <search>case,rhino,model,3dm,polyline,convert,rhynamo</search>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static Dictionary<string, object> Rhino_PolylinesToDS(File3dmObject[] RhinoObjects)
    {
      // list of Rhino Poly curves
      List<Rhino.Geometry.PolylineCurve> rh_polycrv = new List<Rhino.Geometry.PolylineCurve>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in RhinoObjects)
      {
        GeometryBase geo = obj.Geometry;

        // check if the object is a Poly curve
        if (geo is Rhino.Geometry.PolylineCurve)
        {
          // add nurbs curve to Poly list
          Rhino.Geometry.PolylineCurve polycrv = geo as Rhino.Geometry.PolylineCurve;
          rh_polycrv.Add(polycrv);

          // attributes
          m_names.Add(obj.Attributes.Name);
          m_layerindeces.Add(obj.Attributes.LayerIndex);
          m_colors.Add(obj.Attributes.ObjectColor);
          m_guids.Add(obj.Attributes.ObjectId.ToString());
        }
      }

      //convert Rhino NURBS to DesignScript NURBS
      Rhynamo.classes.clsGeometryConversionUtils rh_ds = new Rhynamo.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.PolyCurve> ds_polycrvs = rh_ds.Convert_PolylineToDS(rh_polycrv);

      return new Dictionary<string, object>
              {
                  { "Geometry", ds_polycrvs },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Converts Rhino PolyCurve objects to Dynamo PolyCurves
    /// </summary>
    /// <param name="RhinoObjects">List of Rhino objects to search through.</param>
    /// <returns name="Geometry">Dynamo Geometry</returns>
    /// <returns name="Name">Object Name</returns>
    /// <returns name="LayerIndex">Layer Index</returns>
    /// <returns name="Color">Object color (as System.Color)</returns>
    /// <returns name="GUID">GUID</returns>
    /// <search>case,rhino,model,3dm,polycurve,convert,rhynamo</search>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static Dictionary<string, object> Rhino_PolyCurvesToDS(File3dmObject[] RhinoObjects)
    {
      // list of Rhino Poly curves
      List<Rhino.Geometry.PolyCurve> rh_polycrv = new List<Rhino.Geometry.PolyCurve>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in RhinoObjects)
      {
        GeometryBase geo = obj.Geometry;

        // check if the object is a Poly curve
        if (geo is Rhino.Geometry.PolyCurve)
        {
          // add nurbs curve to Poly list
          Rhino.Geometry.PolyCurve polycrv = geo as Rhino.Geometry.PolyCurve;
          rh_polycrv.Add(polycrv);

          // attributes
          m_names.Add(obj.Attributes.Name);
          m_layerindeces.Add(obj.Attributes.LayerIndex);
          m_colors.Add(obj.Attributes.ObjectColor);
          m_guids.Add(obj.Attributes.ObjectId.ToString());
        }
      }

      //convert Rhino NURBS to DesignScript NURBS
      Rhynamo.classes.clsGeometryConversionUtils rh_ds = new Rhynamo.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.PolyCurve> ds_polycrvs = rh_ds.Convert_PolyCurveToDS(rh_polycrv);

      return new Dictionary<string, object>
              {
                  { "Geometry", ds_polycrvs },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Converts Rhino point objects to Dynamo points.
    /// </summary>
    /// <param name="RhinoObjects">List of Rhino objects to search through.</param>
    /// <returns name="Geometry">Dynamo Geometry</returns>
    /// <returns name="Name">Object Name</returns>
    /// <returns name="LayerIndex">Layer Index</returns>
    /// <returns name="Color">Object color (as System.Color)</returns>
    /// <returns name="GUID">GUID</returns>
    /// <search>case,rhino,model,3dm,point,convert,rhynamo</search>
    [MultiReturn(new[] { "Geometry", "Name", "LayerIndex", "Color", "GUID" })]
    public static Dictionary<string, object> Rhino_PointsToDS(File3dmObject[] RhinoObjects)
    {
      // list of Rhino points
      List<Rhino.Geometry.Point> rh_points = new List<Rhino.Geometry.Point>();

      //Attributes
      List<string> m_names = new List<string>();
      List<int> m_layerindeces = new List<int>();
      List<System.Drawing.Color> m_colors = new List<System.Drawing.Color>();
      List<string> m_guids = new List<string>();

      foreach (File3dmObject obj in RhinoObjects)
      {
        GeometryBase geo = obj.Geometry;

        // check if the geometry is a Point
        if (geo is Rhino.Geometry.Point)
        {
          // add point to Rhino points list
          Rhino.Geometry.Point pt = geo as Rhino.Geometry.Point;
          rh_points.Add(pt);

          // attributes
          m_names.Add(obj.Attributes.Name);
          m_layerindeces.Add(obj.Attributes.LayerIndex);
          m_colors.Add(obj.Attributes.ObjectColor);
          m_guids.Add(obj.Attributes.ObjectId.ToString());
        }
      }
      // convert Rhino points to DesignScript points
      Rhynamo.classes.clsGeometryConversionUtils rh_ds = new Rhynamo.classes.clsGeometryConversionUtils();
      List<Autodesk.DesignScript.Geometry.Point> ds_points = rh_ds.Convert_PointsToDS(rh_points);

      return new Dictionary<string, object>
              {
                  { "Geometry", ds_points },
                  { "Name", m_names },
                  { "LayerIndex", m_layerindeces },
                  { "Color", m_colors },
                  { "GUID", m_guids }
              };
    }

    /// <summary>
    /// Get RGB values form a system color
    /// </summary>
    /// <param name="color">Rhino system color</param>
    /// <returns name="R">Red (0-255)</returns>
    /// <returns name="G">Green (0-255)</returns>
    /// <returns name="B">Blue (0-255)</returns>
    /// <search>case,rhino,model,3dm,color,convert,rhynamo</search>
    [MultiReturn(new[] { "R", "G", "B" })]
    public static Dictionary<string, object> Rhino_ColorRGB(System.Drawing.Color color)
    {
      int R = color.R;
      int G = color.G;
      int B = color.B;

      return new Dictionary<string, object>
      {
        {"R", R},
        {"G", G},
        {"B", B}
      };
    }

    /// <summary>
    /// Converts Rhino text-based objects to Dynamo
    /// </summary>
    /// <param name="RhinoObjects">List of Rhino objects to search through.</param>
    /// <returns name="Text">Text string</returns>
    /// <returns name="Location">Text location</returns>
    /// <returns name="Height">Text height</returns>
    /// <returns>Text information</returns>
    [MultiReturn(new[] { "Text", "Location", "Height" })]
    public static Dictionary<string, object> Rhino_TextToDS(File3dmObject[] RhinoObjects)
    {
      List<Rhino.Geometry.TextEntity> rh_texts = new List<TextEntity>();

      List<string> m_texts = new List<string>();
      List<Autodesk.DesignScript.Geometry.Point> m_locations = new List<Autodesk.DesignScript.Geometry.Point>();
      List<double> m_heights = new List<double>();

      foreach (File3dmObject obj in RhinoObjects)
      {
        GeometryBase geo = obj.Geometry;

        if (geo is Rhino.Geometry.TextEntity) // for Text Entities
        {
          // get text entity
          Rhino.Geometry.TextEntity rh_txt = geo as Rhino.Geometry.TextEntity;
          m_texts.Add(rh_txt.Text);

          // get locations
          Autodesk.DesignScript.Geometry.Point m_loc = Autodesk.DesignScript.Geometry.Point.ByCoordinates(rh_txt.Plane.OriginX, rh_txt.Plane.OriginY, rh_txt.Plane.OriginZ);
          m_locations.Add(m_loc);

          // get text height
          m_heights.Add(rh_txt.TextHeight);
        }
        else if (geo is Rhino.Geometry.TextDot) // for Text Dots
        {
          Rhino.Geometry.TextDot rh_txt = geo as Rhino.Geometry.TextDot;
          m_texts.Add(rh_txt.Text);

          // get locations
          Autodesk.DesignScript.Geometry.Point m_loc = Autodesk.DesignScript.Geometry.Point.ByCoordinates(rh_txt.Point.X, rh_txt.Point.Y, rh_txt.Point.Z);
          m_locations.Add(m_loc);
          // get text height
          m_heights.Add(rh_txt.FontHeight);
        }
      }

      return new Dictionary<string, object>
              {
                  { "Text", m_texts },
                  { "Location", m_locations },
                  { "Height", m_heights }
              };

    }

  }
}
