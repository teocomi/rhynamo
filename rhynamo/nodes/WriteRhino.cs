﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Autodesk.DesignScript.Runtime;
using Autodesk.DesignScript.Geometry;

using Rhino.FileIO;
using Rhino.Geometry;

namespace Interoperability.WriteRhino
{
  /// <summary>
  /// Translate Dynamo Geometry to Rhino Geometry
  /// </summary>
  public static class CreateRhinoGeometry
  {
    /// <summary>
    /// Converts Dynamo PolySurfaces to Rhino Breps
    /// </summary>
    /// <param name="DynamoPolysurface">Dynamo Breps</param>
    /// <returns name="RhinoBrep">Rhino Brep</returns>
    /// <search>case,rhino,3dn,rhynamo,brep</search>
    public static Rhino.Geometry.Brep DS_BrepToRhino(Autodesk.DesignScript.Geometry.PolySurface DynamoPolysurface)
    {
      try
      {
        Rhynamo.classes.clsGeometryConversionUtils ds_rh = new Rhynamo.classes.clsGeometryConversionUtils();
        Rhino.Geometry.Brep rh_brp = ds_rh.Convert_PolySurfaceTo3dm(DynamoPolysurface);
        return rh_brp;
      }
      catch { return null; }
    }

    /// <summary>
    /// Converts Dynamo curves to Rhino curve objects
    /// </summary>
    /// <param name="DynamoCurve">Dynamo Curves</param>
    /// <returns name="RhinoCurve">Rhino Curves</returns>
    /// <search>case,rhino,3dm,rhynamo,curve</search>
    public static Rhino.Geometry.Curve DS_CurvesToRhino(Autodesk.DesignScript.Geometry.Curve DynamoCurve)
    {
      try
      {
        Rhynamo.classes.clsGeometryConversionUtils ds_rh = new Rhynamo.classes.clsGeometryConversionUtils();
        Rhino.Geometry.Curve rh_crv = ds_rh.Convert_CurvesTo3dm(DynamoCurve);
        return rh_crv;
      }
      catch { return null; }
    }

    /// <summary>
    /// Converts Dynamo meshes to Rhino meshes
    /// </summary>
    /// <param name="DynamoMesh">Dynamo Meshes</param>
    /// <returns name="RhinoMesh">Rhino Meshes</returns>
    /// <search>case,rhino,3dm,rhynamo,mesh</search>
    public static Rhino.Geometry.Mesh DS_MeshToRhino(Autodesk.DesignScript.Geometry.Mesh DynamoMesh)
    {
      try
      {
        Rhynamo.classes.clsGeometryConversionUtils ds_rh = new Rhynamo.classes.clsGeometryConversionUtils();
        Rhino.Geometry.Mesh rh_mesh = ds_rh.Convert_MeshTo3dm(DynamoMesh);
        return rh_mesh;
      }
      catch { return null; }
    }

    /// <summary>
    /// Converts Dynamo nurbs surfaces to Rhino nurbs surface objects
    /// </summary>
    /// <param name="DynamoSurface">Dynamo Nurbs Surfaces</param>
    /// <returns name="NurbsSurface">Rhino Nurbs Surfaces</returns>
    /// <search>case,rhino,3dm,rhynamo,nurbssurface</search>
    public static Rhino.Geometry.NurbsSurface DS_NurbsSurfaceToRhino(Object DynamoSurface)
    {
      try
      {
        Rhino.Geometry.NurbsSurface rh_srfs = null;
        if (DynamoSurface is Autodesk.DesignScript.Geometry.NurbsSurface)
        {
          Rhynamo.classes.clsGeometryConversionUtils ds_rh = new Rhynamo.classes.clsGeometryConversionUtils();
          Autodesk.DesignScript.Geometry.NurbsSurface ds_nurbssurface = (Autodesk.DesignScript.Geometry.NurbsSurface)DynamoSurface;
          rh_srfs = ds_rh.Convert_SurfacesTo3dm(ds_nurbssurface);
        }
        else if (DynamoSurface is Autodesk.DesignScript.Geometry.Surface)
        {
          Rhynamo.classes.clsGeometryConversionUtils ds_rh = new Rhynamo.classes.clsGeometryConversionUtils();
          Autodesk.DesignScript.Geometry.Surface ds_srf = (Autodesk.DesignScript.Geometry.Surface)DynamoSurface;
          Autodesk.DesignScript.Geometry.NurbsSurface ds_nurbssurface = ds_srf.ToNurbsSurface();
          rh_srfs = ds_rh.Convert_SurfacesTo3dm(ds_nurbssurface);
        }
        return rh_srfs;
      }
      catch { return null; }
    }

    /// <summary>
    /// Converts Dynamo points to Rhino point objects
    /// </summary>
    /// <param name="DynamoPoint">Dynamo Points</param>
    /// <returns name="RhinoPoint">Rhino Points</returns>
    /// <search>case,rhino,3dm,rhynamo,point</search>
    public static Rhino.Geometry.Point DS_PointsToRhino(Autodesk.DesignScript.Geometry.Point DynamoPoint)
    {
      try
      {
        Rhynamo.classes.clsGeometryConversionUtils ds_rh = new Rhynamo.classes.clsGeometryConversionUtils();
        Rhino.Geometry.Point rh_pt = ds_rh.Convert_PointsTo3dm(DynamoPoint);

        return rh_pt;
      }
      catch { return null; }
    }
  }

  /// <summary>
  /// Create Rhino Objects with Attributes
  /// </summary>
  public static class CreateRhinoObjects
  {
    /// <summary>
    /// Creates a true System color object for Rhino layers and object colors.
    /// </summary>
    /// <param name="Red">Red (0-255)</param>
    /// <param name="Green">Green (0-255)</param>
    /// <param name="Blue">Blue (0-255)</param>
    /// <returns name="SystemColor">System.Drawing.Color</returns>
    /// <search>case,rhino,3dm,rhynamo,layer,color,model</search>
    public static System.Drawing.Color Create_RhinoColor(int Red, int Green, int Blue)
    {
      try
      {
        return System.Drawing.Color.FromArgb(Red, Green, Blue);
      }
      catch { return System.Drawing.Color.FromArgb(0, 0, 0); }
    }

    /// <summary>
    /// Create a Rhino Layer
    /// </summary>
    /// <param name="LayerName">Layer name</param>
    /// <param name="LayerColor">Layer color</param>
    /// <returns name="RhinoLayer">Layer object</returns>
    /// <search>case,rhino,3dm,rhynamo,layer,model</search>
    public static Rhino.DocObjects.Layer Create_RhinoLayer(string LayerName, System.Drawing.Color LayerColor)
    {
      try
      {
        Rhino.DocObjects.Layer m_layer = new Rhino.DocObjects.Layer();
        m_layer.Name = LayerName;
        m_layer.Color = LayerColor;
        return m_layer;
      }
      catch { return null; }
    }

    /// <summary>
    /// Create Rhino Object
    /// </summary>
    /// <param name="RhinoGeometry">Rhino Geometry</param>
    /// <param name="ObjName">Object Name</param>
    /// <param name="ObjColor">Object Color</param>
    /// <param name="ObjLayer">Layer</param>
    /// <returns name="RhinoObject">Rhino Object</returns>
    /// <search>case,rhino,3dm,rhynamo,object,model</search>
    public static Object Create_RhinoObject(Object RhinoGeometry, string ObjName, System.Drawing.Color ObjColor, Rhino.DocObjects.Layer ObjLayer)
    {
      try
      {
        Rhynamo.classes.clsRhinoObject m_rhinoobj = new Rhynamo.classes.clsRhinoObject(RhinoGeometry, ObjLayer, ObjName, ObjColor);
        return (Object)m_rhinoobj;
      }
      catch { return null; }
    } 

    /// <summary>
    /// Sets a User String to an Object using matching keys and values
    /// </summary>
    /// <param name="RhinoObject"></param>
    /// <param name="keys">Keys as strings</param>
    /// <param name="values">Values as strings</param>
    /// <returns name="RhinoObject">Rhino object with user string</returns>
    public static Object Set_UserString(Object RhinoObject, List<string> keys, List<string> values)
    {
      File3dmObject m_obj = (File3dmObject)RhinoObject;

      for (int i = 0; i < keys.Count; i++ )
      {
        m_obj.Geometry.SetUserString(keys[i], values[i]);
      }
       
      return m_obj;
    }

    /// <summary>
    /// Sets a User Dictionary to an Object using matching keys and values
    /// </summary>
    /// <param name="RhinoObject"></param>
    /// <param name="keys">Keys as strings</param>
    /// <param name="values">Values as integer, double, string, guid, or boolean</param>
    /// <returns name="RhinoObject">Rhino object with user string</returns>
    public static Object Set_UserDictionary(Object RhinoObject, List<string> keys, List<object> values)
    {
      File3dmObject m_obj = (File3dmObject)RhinoObject;

      for (int i = 0; i < keys.Count; i++)
      {
        if (values[i] is int)
        {
          m_obj.Geometry.UserDictionary.Set(keys[i], (int)values[i]);
        }
        else if (values[i] is string)
        {
          m_obj.Geometry.UserDictionary.Set(keys[i], (string)values[i]);
        }
        else if (values[i] is double)
        {
          m_obj.Geometry.UserDictionary.Set(keys[i], (double)values[i]);
        }
        else if (values[i] is bool)
        {
          m_obj.Geometry.UserDictionary.Set(keys[i], (bool)values[i]);
        }
        else if (values[i] is Guid)
        {
          m_obj.Geometry.UserDictionary.Set(keys[i], (Guid)values[i]);
        }
        
      }

      return m_obj;
    }
  }

  /// <summary>
  /// Save the Rhino model to a File
  /// </summary>
  public static class SaveRhino3dmModel
  {
    /// <summary>
    /// Save Rhino Model
    /// </summary>
    /// <param name="FilePath">Location of the Rhino File</param>
    /// <param name="SaveBool">Toggle to Save the File</param>
    /// <param name="RhinoObjects">Rhino Geometry to Save</param>
    /// <returns name="RhinoModel">Rhino 3dm Model Object</returns>
    /// <search>case,rhino,3dm,rhynamo,save,model</search>
    public static File3dm Save_Rhino3dmModel(string FilePath, bool SaveBool, Object[] RhinoObjects)
    {
      try
      {
        // Rhino model
        File3dm m_model = new File3dm();

        // Create Default Layer
        Rhino.DocObjects.Layer m_defaultlayer = new Rhino.DocObjects.Layer();
        m_defaultlayer.Name = "Default";
        m_defaultlayer.LayerIndex = 0;
        m_defaultlayer.IsVisible = true;
        m_defaultlayer.IsLocked = false;
        m_defaultlayer.Color = System.Drawing.Color.FromArgb(0, 0, 0);

        // Add default layer to model
        m_model.Layers.Add(m_defaultlayer);
        m_model.Polish();

        //iterate through objects
        for (int i = 0; i < RhinoObjects.Length; i++)
        {
          // Rhino object
          Rhynamo.classes.clsRhinoObject m_rhinoobj = RhinoObjects[i] as Rhynamo.classes.clsRhinoObject;

          // rhino object information
          Object m_objgeo = m_rhinoobj.RhinoObject;
          string m_objname = m_rhinoobj.ObjectName;
          System.Drawing.Color m_objcolor = m_rhinoobj.ObjectColor;
          Rhino.DocObjects.Layer m_objlayer = m_rhinoobj.ObjectLayer;
          m_objlayer.LayerIndex = m_model.Layers.Count;

          // object attributes
          Rhino.DocObjects.ObjectAttributes objatt = new Rhino.DocObjects.ObjectAttributes();
          objatt.Name = m_objname;
          objatt.ObjectColor = m_objcolor;

          if (m_objcolor != m_objlayer.Color)
          {
            objatt.ColorSource = Rhino.DocObjects.ObjectColorSource.ColorFromObject;
          }
          else
          {
            objatt.ColorSource = Rhino.DocObjects.ObjectColorSource.ColorFromLayer;
          }

          // setup layer
          try
          {
            bool m_layerexists = false;
            int m_layerindex = -1;
            for (int j = 0; j < m_model.Layers.Count; j++)
            {
              Rhino.DocObjects.Layer l = m_model.Layers[j];
              if (l.Name == m_objlayer.Name)
              {
                m_layerexists = true;
                m_layerindex = j;
              }
            }

            // if the layer exists
            if (m_layerexists == true)
            {
              // set the object attribute layer index
              objatt.LayerIndex = m_layerindex;
            }
            else
            {
              // add layer to the model and set the object attribute
              m_model.Layers.Add(m_objlayer);
              m_model.Polish();
              objatt.LayerIndex = m_objlayer.LayerIndex;
            }
          }
          catch { }

          // check if point
          if (m_objgeo is Rhino.Geometry.Point)
          {
            Rhino.Geometry.Point pt = (Rhino.Geometry.Point)m_objgeo;
            double x = pt.Location.X;
            double y = pt.Location.Y;
            double z = pt.Location.Z;

            Rhino.Geometry.Point3d pt3d = new Point3d(x, y, z);
            m_model.Objects.AddPoint(pt3d, objatt);
          }

          // check if it is a curve
          if (m_objgeo is Rhino.Geometry.Curve)
          {
            Rhino.Geometry.Curve cv = m_objgeo as Rhino.Geometry.Curve;
            m_model.Objects.AddCurve(cv, objatt);
          }

          //check if surface
          if (m_objgeo is Rhino.Geometry.NurbsSurface)
          {
            Rhino.Geometry.Surface srf = m_objgeo as Rhino.Geometry.Surface;
            m_model.Objects.AddSurface(srf, objatt);
          }

          //check if brep
          if (m_objgeo is Rhino.Geometry.Brep)
          {
            Rhino.Geometry.Brep brp = m_objgeo as Rhino.Geometry.Brep;
            m_model.Objects.AddBrep(brp, objatt);
          }

          //check if mesh
          if (m_objgeo is Rhino.Geometry.Mesh)
          {
            Rhino.Geometry.Mesh msh = m_objgeo as Rhino.Geometry.Mesh;
            m_model.Objects.AddMesh(msh, objatt);
          }
        }

        // write file
        if (SaveBool == true)
        {
          try
          {
            // write model file
            m_model.Write(FilePath, 1);
          }
          catch {  }
        }

        return m_model;
      }
      catch { return null; }
    }

    /// <summary>
    /// Creates a Rhino model
    /// </summary>
    /// <param name="rh_objs">Rhino Objects</param>
    /// <returns>Rhino Model Object</returns>
    /// <search>case,rhino,3dm,rhynamo,model</search>
    //////public static File3dm Create_RhinoModel(Object[] rh_objects)
    //////{
    //////  try
    //////  {
    //////    // Rhino model
    //////    File3dm m_model = new File3dm();

    //////    // Create Default Layer
    //////    Rhino.DocObjects.Layer m_defaultlayer = new Rhino.DocObjects.Layer();
    //////    m_defaultlayer.Name = "Default";
    //////    m_defaultlayer.LayerIndex = 0;
    //////    m_defaultlayer.IsVisible = true;
    //////    m_defaultlayer.IsLocked = false;
    //////    m_defaultlayer.Color = System.Drawing.Color.FromArgb(0, 0, 0);

    //////    // Add default layer to model
    //////    m_model.Layers.Add(m_defaultlayer);
    //////    m_model.Polish();

    //////    //iterate through objects
    //////    for (int i = 0; i < rh_objects.Length; i++)
    //////    {
    //////      // Rhino object
    //////      CASE.classes.clsRhinoObject m_rhinoobj = rh_objects[i] as CASE.classes.clsRhinoObject;

    //////      // rhino object information
    //////      Object m_objgeo = m_rhinoobj.RhinoObject;
    //////      string m_objname = m_rhinoobj.ObjectName;
    //////      System.Drawing.Color m_objcolor = m_rhinoobj.ObjectColor;
    //////      Rhino.DocObjects.Layer m_objlayer = m_rhinoobj.ObjectLayer;
    //////      m_objlayer.LayerIndex = m_model.Layers.Count;

    //////      // object attributes
    //////      Rhino.DocObjects.ObjectAttributes objatt = new Rhino.DocObjects.ObjectAttributes();
    //////      objatt.Name = m_objname;
    //////      objatt.ObjectColor = m_objcolor;

    //////      if (m_objcolor != m_objlayer.Color)
    //////      {
    //////        objatt.ColorSource = Rhino.DocObjects.ObjectColorSource.ColorFromObject;
    //////      }
    //////      else
    //////      {
    //////        objatt.ColorSource = Rhino.DocObjects.ObjectColorSource.ColorFromLayer;
    //////      }

    //////      // setup layer
    //////      try
    //////      {
    //////        bool m_layerexists = false;
    //////        int m_layerindex = -1;
    //////        for (int j = 0; j < m_model.Layers.Count; j++)
    //////        {
    //////          Rhino.DocObjects.Layer l = m_model.Layers[j];
    //////          if (l.Name == m_objlayer.Name)
    //////          {
    //////            m_layerexists = true;
    //////            m_layerindex = j;
    //////          }
    //////        }

    //////        // if the layer exists
    //////        if (m_layerexists == true)
    //////        {
    //////          // set the object attribute layer index
    //////          objatt.LayerIndex = m_layerindex;
    //////        }
    //////        else
    //////        {
    //////          // add layer to the model and set the object attribute
    //////          m_model.Layers.Add(m_objlayer);
    //////          m_model.Polish();
    //////          objatt.LayerIndex = m_objlayer.LayerIndex;
    //////        }
    //////      }
    //////      catch { }

    //////      // check if point
    //////      if (m_objgeo is Rhino.Geometry.Point)
    //////      {
    //////        Rhino.Geometry.Point pt = (Rhino.Geometry.Point)m_objgeo;
    //////        double x = pt.Location.X;
    //////        double y = pt.Location.Y;
    //////        double z = pt.Location.Z;

    //////        Rhino.Geometry.Point3d pt3d = new Point3d(x, y, z);
    //////        m_model.Objects.AddPoint(pt3d, objatt);
    //////      }

    //////      // check if curve
    //////      if (m_objgeo is Rhino.Geometry.Curve)
    //////      {
    //////        Rhino.Geometry.Curve cv = m_objgeo as Rhino.Geometry.Curve;
    //////        m_model.Objects.AddCurve(cv, objatt);
    //////      }

    //////      //check if surface
    //////      if (m_objgeo is Rhino.Geometry.NurbsSurface)
    //////      {
    //////        Rhino.Geometry.Surface srf = m_objgeo as Rhino.Geometry.Surface;
    //////        m_model.Objects.AddSurface(srf, objatt);
    //////      }

    //////      //check if brep
    //////      if (m_objgeo is Rhino.Geometry.Brep)
    //////      {
    //////        Rhino.Geometry.Brep brp = m_objgeo as Rhino.Geometry.Brep;
    //////        m_model.Objects.AddBrep(brp, objatt);
    //////      }

    //////      //check if mesh
    //////      if (m_objgeo is Rhino.Geometry.Mesh)
    //////      {
    //////        Rhino.Geometry.Mesh msh = m_objgeo as Rhino.Geometry.Mesh;
    //////        m_model.Objects.AddMesh(msh, objatt);
    //////      }
    //////    }
    //////    return m_model;
    //////  }
    //////  catch { return null; }

    //////}

    /// <summary>
    /// Save a Rhino Model file to a location
    /// </summary>
    /// <param name="filepath">File path for the 3D model file</param>
    /// <param name="rh_model">Rhino model</param>
    /// <returns name="message">Message, created or failed</returns>
    /// <search>case,rhino,3dm,rhynamo,save</search>
    //////public static string Save_Rhino3dmFile(string filepath, File3dm rh_model)
    //////{
    //////  try
    //////  {
    //////    // write model file
    //////    rh_model.Write(filepath, 1);
    //////    return "Rhino file saved";
    //////  }
    //////  catch (Exception x) { return x.ToString(); }
    //////}
  }
}
